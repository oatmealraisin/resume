build:
	pdflatex resume

clean:
	rm -f resume.pdf *.out *.log *.aux main.pdf

show: clean build
	mupdf resume.pdf

.PHONY: build clean show
